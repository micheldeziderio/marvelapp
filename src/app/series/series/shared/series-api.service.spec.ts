import { TestBed } from '@angular/core/testing';

import { SeriesApiService } from './series-api.service';

describe('SeriesApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeriesApiService = TestBed.get(SeriesApiService);
    expect(service).toBeTruthy();
  });
});
