import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/Operators';


@Injectable({
  providedIn: 'root'
})
export class SeriesApiService {

  PUBLIC_KEY ='e969b875aa88f6e850e617e4b05c5ce5';
  HASH = 'e969b875aa88f6e850e617e4b05c5ce5';
  URL_API = `https://gateway.marvel.com:443/v1/public/series?apikey=${this.PUBLIC_KEY}&hash=${this.HASH}`;

  constructor(private http: HttpClient) { }

  getAllSeries() : Observable<any> {
    return this.http.get<any>(this.URL_API)
    .pipe( map((data :any) => data.data.results) )
  }
}
