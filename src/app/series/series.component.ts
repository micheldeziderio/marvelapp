import { Component, OnInit } from '@angular/core';
import { SeriesApiService } from './series/shared/series-api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.scss']
})
export class SeriesComponent implements OnInit {

  constructor(private ObservableSeries: SeriesApiService) { }

  allSeries: Observable<any>;

  ngOnInit() {

    this.getSeries();

  }

  getSeries(){
    this.allSeries = this.ObservableSeries.getAllSeries();
  }
}
