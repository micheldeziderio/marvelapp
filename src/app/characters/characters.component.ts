import { Component, OnInit } from '@angular/core';
import { CharactersApiService } from './character/shared/characters-api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  constructor(private ObservableSrc: CharactersApiService) { }

  allCharacters: Observable<any>;

  ngOnInit() {

    this.getCharacter();

  }

  getCharacter() {
    this.allCharacters = this.ObservableSrc.getAllCharacters();
  }

}
